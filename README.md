# Zonar Python Empty Box Fast Api 
> this has been adapted from the https://gitlab.com/ZonarSystems/zonar-base/python-empty-box changes will continue 

## Overview
this is a simple python "Hello World!" fastapi app demonstrating Zonar Standards on the following: 

 - General python folder structure
 - Gitlab CI/CD pipeline using the Fairwinds Ops runner and standards
 - Dockerfile 
 - Datadog integration
 - Installation from Artifactory PyPI repository
 - Terraform folder structure
- Terraform with [Terrabox](https://gitlab.com/ZonarSystems/zonar-base/terrabox)

> This project uses the internal package `ds-toolbox` see docs [here](https://zonarsystems.gitlab.io/data-services/toolbox/index.html).


## Table of Contents

1. [Prerequisites](#prerequisites)
2. [Steps to Change](#steps-to-change)
3. [CI/CD Stages](#CI/CD-stages)
4. [Required Variables](#required-variables)
5. [Local Development](#local-development)
6. [Running Tests](#running-tests)
7. [Folder Structure](#folder-structure)
8. [Empty Box Standards](#empty-box-standards)
9. [Datadog](#datadog)
10. [Artifactory](#artifactory)
11. [Additional Documentation](#additional-documentation)

## Prerequisites



## Steps to Change


## CI/CD Stages

### Required Variables


## Local Development

```
source env/bin/activate
uvicorn main:app --reload
```

## Running Tests

## Folder Structure


## Empty Box Standards
## Datadog

## Artifactory
JFrog Artifactory includes a PyPI repository used for hosting internal Python packages. By publishing packages to
Artifactory, they are made available to other projects and can be included in a project's `requirements.txt` to be
installed via `pip`, just like any other dependency.

## Additional Documentation
More documentation can be found here:

- [Zonar Cloud Wiki](https://gitlab.com/ZonarSystems/zonar-base/zonar-cloud-developers-guide/blob/master/readme.md)
- [Helm](https://docs.helm.sh/)
- [Fairwinds Runner](https://github.com/reactiveops/rok8s-scripts)
- [Datadog and StatsD](https://docs.datadoghq.com/developers/dogstatsd/)
- [JFrog Artifactory PyPI](https://www.jfrog.com/confluence/display/RTF/PyPI+Repositories#PyPIRepositories-ResolvingfromArtifactoryUsingPIP)
- [Terraform](https://www.terraform.io)
- [Terrabox](https://gitlab.com/ZonarSystems/zonar-base/terrabox)


## TODO
Additional best practices that need to be implemented:
- Secret Manager

