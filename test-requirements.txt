pytest==6.2.4
pytest-asyncio==0.15.1
pytest-cov==2.12.1
coverage[toml]==5.5
httpx==0.18.1
requests==2.25.1
mypy
