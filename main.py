from fastapi import FastAPI ##python class that has everything you need for the API 
from typing import Optional
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder


class Item(BaseModel):
    name:str
    description: Optional[str] = None 
    price: float
    tax: Optional[float] = None 

app = FastAPI() # creates a FASTAPI instance
fake_db = {}


"""
Here the app variable will be an instance of the class FastAPI
This will be the main point of interaction to create all your API
"""
@app.get("/")
async def root():
    return{"greeting": "Hello You",
           "youve successfull": "connected to this endpoint",
           "now": "finish the rest"}

"""Order Matters! When creating path operations,(Fixed paths first!)
you can find situations where you have a fixed path. 
  Returns:
  _type_: _description_
"""
@app.get("/users/me")
async def read_user_me(): 
    return {"user_id": "the current user"}

@app.get("/items/{item_id}")
async def read_item(item_id):
    return{"item_id": item_id}

"""
Here is where we'll have the examples from other apis
Plan is to see how they do it. Calling the firehose api rather than big query or whatever
we choose as our db 
"""
@app.post("/items/")
async def create_item(item: Item):
    return item

@app.put("/items/{id}")
def update_item(id: str, item: Item):
    json_compatible_item_data = jsonable_encoder(item)
    fake_db[id] = json_compatible_item_data
    
