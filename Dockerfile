FROM python:3.8

MAINTAINER {POD-NAME} <{POD-EMAIL}>

# Install system dependencies
RUN apt-get update
RUN apt-get install -y libgeos-dev libpq-dev libev-dev

# Setup pip to install from artifactory
ARG PIPCONF_B64
RUN mkdir -p ~/.pip && echo $PIPCONF_B64 | base64 -d > ~/.pip/pip.conf

# Copy over the requirements.txt so we can install it
COPY requirements.txt /requirements.txt

# Install requirements
RUN pip3 install --upgrade pip
RUN pip3 install -r /requirements.txt

# Copy project files and test files
COPY python_empty_box /opt/zonar/python-empty-box/python_empty_box
COPY tests /opt/zonar/python-empty-box/tests

WORKDIR /opt/zonar/python-empty-box
ENV PYTHONPATH="${PYTHONPATH}:/opt/zonar/python-empty-box"