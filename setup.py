from setuptools import setup, find_packages
from typing import Dict

PACKAGE = "{{PROJECT}}-api"
MODULE = "{{PROJECT}}_api"

version_globals: Dict[str, object] = {}
exec(open(f"{MODULE}/_version.py").read(), version_globals)
VERSION = version_globals.get("__version__", "0.1.0")

with open("requirements.txt") as f:
    requirements = [x.strip() for x in f.readlines() if "://" not in x]

with open("test-requirements.txt") as f:
    test_requirements = [x.strip() for x in f.readlines() if "://" not in x]

setup(
    name=PACKAGE,
    version=VERSION,
    packages=find_packages(),
    install_requires=requirements,
    tests_require=test_requirements,
)
