#!/bin/bash

# Magic to make things work on mac
export LC_CTYPE=C
export LANG=C

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. Must supply 2 arguments: project_name and team_name"
    exit 1
fi

if [ $# -eq 1 ]
  then
    echo "Only 1 argument supplied. Must supply 2 arguments: project_name and team_name"
    exit 1
fi

if [ $# > 2 ]
  then
    echo "Too many arguments supplied. Must supply 2 arguments: project_name and team_name"
    exit 1
fi

project_name_underscore="${1//-/_}"
project_name_hyphen="${1//_/-}"
team_name_underscore="${2//-/_}"
team_name_hyphen="${2//_/-}"

# Rename dirs
mv ./python_empty_box $project_name_underscore
mv ./deploy/charts/python-empty-box ./deploy/charts/$project_name_hyphen

# Rename files
find . -not -path '*/\.*' -type f -name 'python-empty-box*' -exec rename "s/python-empty-box/$project_name_hyphen/" {} \;

# Replace file / directory name references
find . -not -path '*/\.*' -not -path '*ename_project.sh' -type f -exec perl -i -pe "s/python-empty-box/$project_name_hyphen/g" {} \; -exec perl -i -pe "s/python_empty_box/$project_name_underscore/g" {} \;
find . -not -path '*/\.*' -not -path '*ename_project.sh' -type f -exec perl -i -pe "s/zonar-team/$team_name_hyphen/g" {} \; -exec perl -i -pe "s/zonar-team/$team_name_underscore/g" {} \;
