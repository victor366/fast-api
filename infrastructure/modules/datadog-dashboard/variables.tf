variable "location" {
  type = string
  description = "Location of resources"
  validation {
    condition = can(index(["na", "eu"], var.location))
    error_message = "Location must be either 'na' or 'eu'."
  }
}