module "dashboard" {
  source = "git::git@gitlab.com:ZonarSystems/data-services/terrabox.git//modules/dynamic-datadog-dashboard?ref=tags/v2.1.13"

  title = "Zonar Python Empty Box"
  layout_type = "ordered"

  template_variables = [
    {
      name = "pubsub_topic"
      tag = "topic_id"
    },
    {
      name = "pubsub_subscription"
      tag = "subscription_id"
    }
  ]

  template_variable_presets = [
    {
      name = "Development"
      default = true
      variables = {
        env = "development"
        deployment = "zonar-team-python-empty-box-dev-deployment"
        ingress = "zonar-team-python-empty-box-dev-ingress"
        kube_cluster = var.location == "na" ? "zonar-working-na-2" : "zonar-working-eu-1"
        pubsub_topic = "zonar-base-topic-test-dev-${var.location}"
        pubsub_subscription = "zonar-base-sub-test-dev-${var.location}"
      }
    },
    {
      name = "QA"
      default = false
      variables = {
        env = "qa"
        deployment = "zonar-team-python-empty-box-qa-deployment"
        ingress = "zonar-team-python-empty-box-qa-ingress"
        kube_cluster = var.location == "na" ? "zonar-working-na-2" : "zonar-working-eu-1"
        pubsub_topic = "zonar-base-topic-test-qa-${var.location}"
        pubsub_subscription = "zonar-base-sub-test-qa-${var.location}"
      }
    },
    {
      name = "Production"
      default = false
      variables = {
        env = "production"
        deployment = "zonar-team-python-empty-box-prod-deployment"
        ingress = "zonar-team-python-empty-box-prod-ingress"
        kube_cluster = var.location == "na" ? "zonar-production-na-2" : "zonar-production-eu-1"
        pubsub_topic = "zonar-base-topic-test-prod-${var.location}"
        pubsub_subscription = "zonar-base-sub-test-prod-${var.location}"
      }
    },
  ]

  widgets = [
    {
      preset = "nginx_ingress"
      context = {
        app = ""
      }
    },
    {
      template = "${path.module}/pubsub.json"
      context = {}
    },
    {
      preset = "indexed_logs"
      context = {
        app = ""
        query = {
          kube_cluster = null
          deployment = null
          service = "python-empty-box"
        }
      }
    },
    {
      preset = "log_links"
      context = {
        app = ""
        log_links = [
          {
            env = "Dev"
            datadog_query = {
              service = "python-empty-box"
              display_container_name = "zonar-team-python-empty-box-dev*"
            }
            stackdriver_query = {}
          },
          {
            env = "QA"
            datadog_query = {
              service = "python-empty-box"
              display_container_name = "zonar-team-python-empty-box-qa*"
            }
            stackdriver_query = {}
          },
          {
            env = "Prod"
            datadog_query = {
              service = "python-empty-box"
              display_container_name = "zonar-team-python-empty-box-prod*"
            }
            stackdriver_query = {}
          }
        ]
      }
    },
    {
      preset = "kubernetes"
      context = {
        release_event = "zonar_team_python_empty_box"
        deployment_type = "deployment"
        app = ""
      }
    }
  ]
}
