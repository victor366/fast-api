# datadog-monitors module

Inputs:
- `environ` - name of the environment to create resources for
- `google_project_id` - name of the Google Cloud Project where resources are deployed
- `notifiers` - list of recipients to notify when monitors alert/resolve
- `ingress` - name of ingress to monitor for errors

Creates the following resources:
- `datadog_monitor` - for high error response rate
