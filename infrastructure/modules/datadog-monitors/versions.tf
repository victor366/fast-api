terraform {
  required_providers {
    datadog = {
      source = "DataDog/datadog"
      version =  ">= 2.19.1"
    }
  }
  required_version = ">= 0.13"
}
