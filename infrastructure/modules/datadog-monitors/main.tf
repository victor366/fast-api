# https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/monitor
locals {
  team = "zonar-team"
  monitor_tags = [
    "env:${var.environ}",
    "team:${local.team}",
    "service:python-empty-box",
  ]
}

resource "datadog_monitor" "high_error_response_rate" {
  name = "${local.team} - Python Empty Box Error Rate High in ${title(var.environ)}"
  type = "query alert"

  query = "avg(last_5m):( sum:nginx_ingress.controller.requests{ingress:${var.ingress},status:5*} / sum:nginx_ingress.controller.requests{ingress:${var.ingress}} ) * 100 > ${var.alert_thresholds["high_error_response_rate"]}"

  monitor_thresholds {
    critical = var.alert_thresholds["high_error_response_rate"]
  }

  require_full_window = false

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has a high error rate.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box error rate has returned to normal.
  {{/is_alert_recovery}}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "high_error_response_count" {
  name = "${local.team} Python Empty Box Error Count High in ${title(var.environ)}"
  type = "query alert"

  query = "sum(last_5m):sum:nginx_ingress.controller.requests{ingress:${var.ingress},status:5*}.as_count() > ${var.alert_thresholds["high_error_response_count"]}"

  monitor_thresholds {
    critical = var.alert_thresholds["high_error_response_count"]
  }

  require_full_window = false

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has a high error count.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box error count has returned to normal.
  {{/is_alert_recovery}}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "high_error_response_composite" {
  name = "${local.team} Python Empty Box Error Rate and Count High in ${title(var.environ)}"
  type = "composite"

  query = "${datadog_monitor.high_error_response_rate.id} && ${datadog_monitor.high_error_response_count.id}"

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has a high error rate and count.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box error rate and count has returned to normal.
  {{/is_alert_recovery}}

  Notify:
  %{ for n in var.notifiers_high_pri ~}
  @${n}
  %{ endfor ~}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "high_number_of_restarts" {
  name = "${local.team} Python Empty Box Number of Restarts High in ${title(var.environ)}"
  type = "metric alert"

  query = "avg(last_10m):per_minute(sum:kubernetes.containers.restarts{kube_deployment:${var.deployment}}) > ${var.alert_thresholds["high_number_of_restarts"]}"

  monitor_thresholds {
    critical = var.alert_thresholds["high_number_of_restarts"]
  }

  require_full_window = false

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has a high number of restarts.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box number of restarts has returned to normal.
  {{/is_alert_recovery}}

  Notify:
  %{ for n in var.notifiers_high_pri ~}
  @${n}
  %{ endfor ~}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "p99_response_time" {
  name = "${local.team} Python Empty Box p99 Response Time High in ${title(var.environ)}"
  type = "metric alert"

  query = "min(last_10m):p99:trace.servlet.request{service:python-empty-box,env:${var.environ}} by {resource_name} > ${var.alert_thresholds["p99_response_time"]}"

  monitor_thresholds {
    critical = var.alert_thresholds["p99_response_time"]
  }

  require_full_window = true

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has a high p99 latency.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box p99 latency has returned to normal.
  {{/is_alert_recovery}}

  Notify:
  %{ for n in var.notifiers_low_pri ~}
  @${n}
  %{ endfor ~}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "cert_expiring" {
  name = "${local.team} Python Empty Box Cert Expiring in ${title(var.environ)}"
  type = "service check"

  query = "\"http.ssl_cert\".over(\"instance:${local.team}_python_empty_box_${var.environ_short}_http_check\",\"url:${var.cert_url}\").by(\"url\").last(6).count_by_status()"
  monitor_thresholds {
    critical = 5
    warning = 2
    ok = 1
  }

  require_full_window = false
  new_host_delay = 300
  notify_no_data = false

  message = <<-EOT
  {{#is_warning}}
  The SSL certificate for {{url.name}} is 2 weeks from expiring.
  {{/is_warning}}

  {{#is_alert}}
  The SSL certificate for {{url.name}} has expired.
  {{/is_alert}}

  {{#is_recovery}}
  The SSL certificate for {{url.name}} has been renewed.
  {{/is_recovery}}

  Check status: {{check_message}}

  Notify:
  %{ for n in var.notifiers_low_pri ~}
  @${n}
  %{ endfor ~}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "high_number_of_pods" {
  name = "${local.team} Python Empty Box Has a High Number of Pods in ${title(var.environ)}"
  type = "metric alert"

  query = "min(last_60m):max:kubernetes_state.deployment.replicas_available{kube_deployment:${var.deployment}} + max:kubernetes_state.deployment.replicas_unavailable{kube_deployment:${var.deployment}}>= ${var.alert_thresholds["high_number_of_pods"]}"
  monitor_thresholds {
    critical = var.alert_thresholds["high_number_of_pods"]
  }

  require_full_window = false

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has a high number of pods.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box pod count has returned to normal.
  {{/is_alert_recovery}}

  Notify:
  %{ for n in var.notifiers_low_pri ~}
  @${n}
  %{ endfor ~}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}

resource "datadog_monitor" "no_pods" {
  name = "${local.team} Python Empty Box Has a No Pods in ${title(var.environ)}"
  type = "metric alert"

  query = "sum(last_1m):sum:kubernetes.pods.running{kube_deployment:${var.deployment}} <= 0"

  monitor_thresholds {
    critical = 0
  }

  require_full_window = false

  message = <<-EOT
  {{#is_alert}}
  The ${var.environ} Python Empty Box has no pods.

  Check the dashboard and logs to diagnose
  {{/is_alert}}

  {{#is_alert_recovery}}
  The ${var.environ} Python Empty Box pod count has returned to normal.
  {{/is_alert_recovery}}

  Notify:
  %{ for n in var.notifiers_high_pri ~}
  @${n}
  %{ endfor ~}

  > Managed with Terraform
  EOT

  tags = local.monitor_tags
}
