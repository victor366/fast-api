variable "environ" {
  type = string
  description = "Name of the environment to create resources for"
}

variable "environ_short" {
  type = string
  description = "Shortened name of the environment to create resources for"
}

variable "google_project_id" {
  type = string
  description = "Name of the Google Cloud Project where resources are deployed"
}

variable "notifiers_high_pri" {
  type = list(string)
  description = "List of recipients to notify for high priority alerts when monitors alert/resolve"
}

variable "notifiers_low_pri" {
  type = list(string)
  description = "List of recipients to notify for low priority alerts when monitors alert/resolve"
}

variable "ingress" {
  type = string
  description = "Name of ingress to monitor"
}

variable "deployment" {
  type = string
  description = "Name of deployment to monitor"
}

variable "cert_url" {
  type = string
  description = "URL to to check certs for"
}

variable "alert_thresholds" {
  type = map(string)
  description = "Map of alert names to threshold values"
}
