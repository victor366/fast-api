## Standard Configs ##
variable "service" {
  description = "Name of the service the infrastructure is for"
}
variable "environment" {
  type = string
  description = "Deployment environment"
  validation {
    condition = can(index(["local", "development", "qa", "staging", "production"], var.environment))
    error_message = "Environment must be either 'local', 'dev', 'qa', or 'prod'."
  }
}
variable "environment_short" {
  type = string
  description = "Deployment environment"
  validation {
    condition = can(index(["local", "dev", "qa", "stage", "prod"], var.environment_short))
    error_message = "Environment must be either 'local', 'dev', 'qa', or 'prod'."
  }
}
variable "project" {
  type = string
  description = "Name of the Google Cloud Project infrastructure is created in"
}
variable "region" {
  type = string
  description = "Region of the Google Cloud Project"
}
variable "zone" {
  type = string
  description = "Zone of the Google Cloud Project"
}
variable "location" {
  type = string
  description = "Location of resources"
  validation {
    condition = can(index(["na", "eu"], var.location))
    error_message = "Location must be either 'na' or 'eu'."
  }
}
variable "cost_allocation" {
  type = string
  description = "Cost allocation ID"
}

## Kubernetes config for Workload Identity ##
variable "deploy_project" {
  description = "Name of the Google Cloud Project the application is deployed to"
}

## Application Configs ##
variable "pubsub" {
  description = "Pubsub topic/subscription settings"
}

## Datadog Monitors ##
variable "datadog_monitor_notifiers_high_pri" {
  type = list(string)
  description = "List of recipients to notify for high priority alerts when monitors alert/resolve"
}
variable "datadog_monitor_notifiers_low_pri" {
  type = list(string)
  description = "List of recipients to notify for low priority alerts when monitors alert/resolve"
}
variable "datadog_monitor_ingress" {
  type = string
  description = "Name of ingress to monitor"
}
variable "datadog_monitor_deployment" {
  type = string
  description = "Name of deployment to monitor"
}
variable "datadog_monitor_cert_url" {
  type = string
  description = "URL to to check certs for"
}
variable "datadog_monitor_alert_thresholds" {
  type = map(string)
  description = "Map of alert names to threshold values"
}
