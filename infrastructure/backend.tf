terraform {
  required_version = ">= 1.0.8" # see https://releases.hashicorp.com/terraform/

  backend "gcs" {
    # bucket and prefix defined in environments/<env>/backend.tfvars
  }

  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.54.0"
    }

    datadog = {
      source = "DataDog/datadog"
      version = "3.0.0"
    }
  }
}