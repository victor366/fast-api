## Standard Configs ##
environment = "qa"
environment_short = "qa"
project = "zonar-working-eu-7bee"
region = "europe-west1"
zone = "europe-west1-c"
location = "eu"

## Kubernetes config for Workload Identity ##
deploy_project = "zonar-working-eu-7bee"

## Datadog Monitors ##
datadog_monitor_notifiers_high_pri = []
datadog_monitor_notifiers_low_pri = []
datadog_monitor_ingress = "zonar-team-python-empty-box-qa-ingress"
datadog_monitor_deployment = "zonar-team-python-empty-box-qa-deployment"
datadog_monitor_cert_url = "https://python-empty-box.qa.eu.zonarsystems.net"
datadog_monitor_alert_thresholds = {
  "high_error_response_rate": 8,
  "high_error_response_count": 10,
  "high_number_of_restarts": 10,
  "p99_response_time": 60,
  "high_number_of_pods": 2
}
