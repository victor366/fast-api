## Standard Configs ##
environment = "development"
environment_short = "dev"
project = "zonar-working-na-e22c"
region = "us-central1"
zone = "us-central1-c"
location = "na"

## Kubernetes config for Workload Identity ##
deploy_project = "zonar-working-na-e22c"

## Datadog Monitors ##
datadog_monitor_notifiers_high_pri = []
datadog_monitor_notifiers_low_pri = []
datadog_monitor_ingress = "zonar-team-python-empty-box-dev-ingress"
datadog_monitor_deployment = "zonar-team-python-empty-box-dev-deployment"
datadog_monitor_cert_url = "https://python-empty-box.dev.zonarsystems.net"
datadog_monitor_alert_thresholds = {
  "high_error_response_rate": 10,
  "high_error_response_count": 10,
  "high_number_of_restarts": 10,
  "p99_response_time": 60,
  "high_number_of_pods": 2,
}
