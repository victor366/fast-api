## Standard Configs ##
environment = "production"
environment_short = "prod"
project = "zonar-production-eu-0417"
region = "europe-west1"
zone = "europe-west1-c"
location = "eu"

## Kubernetes config for Workload Identity ##
deploy_project = "zonar-production-eu-0417"

## Datadog Monitors ##
datadog_monitor_notifiers_high_pri = []
datadog_monitor_notifiers_low_pri = []
datadog_monitor_ingress = "zonar-team-python-empty-box-prod-ingress"
datadog_monitor_deployment = "zonar-team-python-empty-box-prod-deployment"
datadog_monitor_cert_url = "https://python-empty-box.production.zonarsystems.net"
datadog_monitor_alert_thresholds = {
  "high_error_response_rate": 0.5,
  "high_error_response_count": 20,
  "high_number_of_restarts": 5,
  "p99_response_time": 3,
  "high_number_of_pods": 10
}
