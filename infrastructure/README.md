# Terraform-managed Infrastructure

This directory contains infrastructure as code, with state maintained via Terraform in Google Cloud Storage.

## Setup

To run, you will need to:

* [Install Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)
* [Install Google Cloud SDK](https://cloud.google.com/sdk/install)
* Authenticate to GCP using `gcloud auth login`
* Setup SOPS

### [Install Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)

For the exact version required, look in a `*/backend.tf` file, at the
`required_version` parameter found in the `terraform` block.

> On a Mac, can install via Homebrew
> ```bash
> $ brew install terraform
> ```

### [Install the Google Cloud SDK](https://cloud.google.com/sdk/install)

This allows the Terraform GCP provider to authenticate.

> On a Mac, can install via Homebrew
> ```bash
> $ brew install google-cloud-sdk
> ```


### Authenticate to GCP using `gcloud auth`

1. Run `gcloud auth login` 

    This will open a web browser where you will be prompted to authorize the Google
    Cloud SDK to your google account. Be sure to use your @zonarsystems.com Google
    account.

2. Set your project with `gcloud config set project <project-id>`

    For example, `<project-id>` for the data-pipeline-qa project is just
    `data-pipeline-qa.`
  
3. Run `gcloud auth application-default login`

    This may prompt you again.

## Deploying Changes with Terraform Using the Pipeline

After making changes to infrastructure within this repo, you can deploy your
changes/additions to development, staging, and production through Gitlab via the 
pipeline. 

This pipeline has two steps: `Terraform Validate and Plan`, and `Terraform Apply`. 

1. `Validate and Plan`: This first step initializes the backend for the environment
you will be deploying, checks whether your infrastructure changes are valid, and runs 
`terraform plan`. `terraform plan` performs a dry run of your deploy, and creates a 
`planfile` within the pipeline artifacts as output. A `planfile` shows the changes that
will be made by what you have written. Once the `Validate and Plan` step is complete,
examine the output in the Gitlab pipeline logs. Check to make sure the changes run
by `terraform plan` are correct before moving on. 

2. `Apply`: Once you've checked that your `Validate and Plan` output is correct, go to
Gitlab to manually kick off the `Apply` step. This step modifies your infrastructure,
making the changes described in your `planfile` and your Terraform code.

## Rolling Back Terraform Changes

If you have deployed Terraform changes using the Gitlab CI/CD pipeline, you can roll back
using the GUI. Find the pipeline in Gitlab history with the state you wish to roll back to,
then manually re-run both Terraform pipeline steps — first `Validate and Plan`, then `Apply`.

## Manual/Local Terraform Usage

In general, Terraform changes should be executed via the Gitlab pipeline. However, if
there are actions needed that can't be completed via the pipeline steps, you can also
run Terraform commands locally from within the `infrastructure` directory.

### Deploying Changes to Infrastructure (Local)

1. **terraform init**

    In order to initialize the backend for the environment you will be deploying, 
    it is necessary to run `terraform init` with the `-backend-config`. This downloads 
    backend providers and sets up links to the modules in the `modules` directory. 
    Run this file from the root `infrastructure` directory.
    
    ```bash
    terraform init -backend-config ./environments/development/backend.tfvars
    ```
    
    If Terraform asks `Do you want to copy existing state to the new backend?`, 
    answer `no`. This happens when you switch the environment you are deploying.

2. **terraform plan**

    This performs a dry run so you can preview the changes Terraform would make to
    your infrastructure should you run a `terraform apply`. In addition to printing
    the computed changes, this command can also generate a `.tfplan` file which can
    be passed to a `terraform apply` directly.
    
    Use the `-var-file` flag to specify which environment for which you want to
    generate a plan. To use this command with the below `terraform apply` command,
    use the `-out` flag to save the plan to a file. 
   
    Before we can run a plan, there are a few environment variables we need to set.
    
    ```bash
   export DD_API_KEY={key} # key can be found in gitlab variables for the data-services group 
   export DD_APP_KEY={key} # key can be found in gitlab variables for the data-services group 
   ```
    
    ```bash
    terraform plan -out mychanges.tfplan -var-file ./environments/development/variables.tfvars
    ```
    
    Plan files should be ignored by git and not checked into the repository as they
    may contain sensitive information. To look at a previously generated plan file,
    use:
    
    ```bash
    terraform plan mychanges.tfplan
    ```

3. **terraform apply**

    This shows output similar to a plan, and will modify your infrastructure to
    match the state described in Terraform code. If you previously saved plan output
    to be used with apply, include that file name, for example: 
    
    ```bash
    terraform apply mychanges.tfplan
    ```

### Viewing Output (Local)

1. **terraform output** 

    This shows the values of any outputs defined in Terraform code. Outputs are
    sometimes used to show a Terraform operator information they need, about
    resources Terraform has created.
    
     ```bash
    terraform output
    ```

### Deleting a Terraform resource (Local)
1. **resource_type and resource_name**

    To delete a Terraform resource, first find the `resource_type` and `resource_name`.
    Those are located in the resource's `.tf` file. (EX: `modules/location-db/firebase.tf`)
    
    They are declared like this:
    ```hcl
    resource "resource_type" "resource_name" {
      ...
    }
    ```
2. **terraform plan**
    To delete the resource, run `plan` with the `--destroy` tag.
    
    ```bash
    terraform plan -out mychanges.tfplan -var-file ./environments/development/variables.tfvars -destroy -target <RESOURCE_TYPE>.<RESOURCE_NAME>
    ```
    You should see the resource to be destroyed listed in red with `~ destroy` as the 
    resource action. 

4. **terraform apply**
        
    If the output from `plan` looks correct, run `apply` as per normal.
    
    ```bash
    terraform apply mychanges.tfplan 
    ```

