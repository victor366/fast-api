## Standard config ##

service = "zonar-base"
cost_allocation = "zonar"

## Application Configs ##

# We'll put this in the auto.tfvars because each env will need a topic/sub with this name.
# Often this will not be the case and it'll be in the variables.tf for each env.
pubsub = {
  test = {  # This is the topic name
    pull_subscriptions = [
      {
        name                 = "test"
        ack_deadline_seconds = 10
        expiration_policy    = ""
      }
    ]
  }
}
