provider "google" {
  credentials = file("terraform-credentials.json")

  project = var.project
  region  = var.region
  zone    = var.zone
}

provider "datadog" {
  api_url = {
    na = "https://api.datadoghq.com/"
    eu = "https://api.datadoghq.eu/"
  }[var.location]
}
