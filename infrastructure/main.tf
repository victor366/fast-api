
# Service account
module "python_empty_box_service_account" {
  source = "git::git@gitlab.com:ZonarSystems/zonar-base/terrabox.git//modules/service-account?ref=tags/v2.1.13"

  display_name = "Python Empty Box ${var.environment_short}"
  description  = "Service account for the Python Empty Box in ${var.environment_short}"

  name         = "py-mty-bx"  // need abbreviate this due to name length restrictions
  service      = var.service
  environment  = var.environment_short
  location     = var.location

  # Use the principle of least privileged access.
  # Assign permissions directly to resources instead of at a project level where possible (unlike this example)
  project_roles = [
    "roles/pubsub.publisher",
    "roles/pubsub.subscriber"
  ]

  generate_workload_identity = {
    workload   = "zonar-team-python-empty-box-${var.environment_short}-sa"  // must match the name of the kubernetes object service account
    namespace  = "zonar-${var.environment}"  // TODO: update this, you will not be deploying to the zonar-{env} namespace
    project_id = var.deploy_project
  }
}

# Pubsub Topic and Subscription
module "pubsub" {
  source = "git::git@gitlab.com:ZonarSystems/zonar-base/terrabox.git//modules/pubsub?ref=tags/v2.1.13"

  for_each = var.pubsub

  service         = var.service
  environment     = var.environment_short
  location        = var.location
  cost_allocation = var.cost_allocation
  region          = var.region
  project_id      = var.project

  name            = each.key
  pull_subscriptions = each.value.pull_subscriptions
 }

# Datadog Dashboard
module "datadog_dashboard" {
  source = "./modules/datadog-dashboard"
  count = var.environment_short == "dev" ? 1 : 0

  location = var.location
}

# Datadog Monitors and Alerts
module "datadog_monitors" {
  source = "./modules/datadog-monitors"

  environ = var.environment
  environ_short = var.environment_short
  google_project_id = var.project
  notifiers_high_pri = var.datadog_monitor_notifiers_high_pri
  notifiers_low_pri = var.datadog_monitor_notifiers_low_pri
  ingress = var.datadog_monitor_ingress
  deployment = var.datadog_monitor_deployment
  cert_url = var.datadog_monitor_cert_url
  alert_thresholds = var.datadog_monitor_alert_thresholds
}
